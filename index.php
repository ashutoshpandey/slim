<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$config = array();

$app = new \Slim\App(["settings" => $config]);

unset($app->getContainer()['errorHandler']);

$container = $app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer("templates/");

/********************* now our configuration ******************/

$app->get('/hello/{name}', function (Request $request, Response $response, $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");
});

$app->get('/about', function (Request $request, Response $response, $args) {
    
    $rootUri = $request->getUri()->getBaseUrl();

    $response = $this->view->render($response, "about.phtml", array('root' => $rootUri));
});

$app->get('/info/{name}[/{age}]', function (Request $request, Response $response, $args) {

    $rootUri = $request->getUri()->getBaseUrl();

	if(!isset($args['age']))
		$args['age'] = 0;
	
    $response = $this->view->render($response, "info.phtml", 
									array(
										'root' => $rootUri,
										'name' => $args['name'],
										'age' => $args['age']
									)
								);
});
/********************* others *********************/

$app->run();